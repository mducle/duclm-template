import React, { Component } from 'react';
import _get from 'lodash-es/get';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import IconFlash from 'sendo-buyers-common/components/Icons/IconFlash';
import IconClock from 'sendo-buyers-common/components/Icons/IconClock';
import CaretUp from 'sendo-buyers-common/components/Icons/CaretUp';

import { getCurrentTime } from 'sendo-web-sdk/helpers/common';
import Swiper from 'sendo-buyers-common/components/Swiper';

import Countdown from './Countdown';
import FlashSaleItem from './FlashSaleItem';
import styles from './flashSales.scss';

const idSelector = 'flashdeals';

export class FlashDeals extends Component {
  constructor(props) {
    super(props);
    this.updateSwiperSetting(_get(props, 'deals', []));
  }
  updateSwiperSetting = deals => {
    this.sliderSetting = {
      slidesPerView: 6.5,
      // virtual: {
      //   slides: deals,
      //   renderExternal: data => {
      //     this.setState({
      //       virtualData: data,
      //     });
      //   },
      // },
      breakpoints: {
        1024: {
          slidesPerView: 4.5,
        },
        1366: {
          slidesPerView: 5.5,
        },
        1600: {
          slidesPerView: 6.5,
        },
      },
    };
  };

  componentWillReceiveProps = nextProps => {
    this.updateSwiperSetting(_get(nextProps, 'deals', []));
  };

  render() {
    const { isSenMall, deals } = this.props;
    const endTime = this.props.metaData.endTime;
    const currentTime = getCurrentTime();
    if (endTime === 0 || endTime <= currentTime) {
      return null;
    }

    if (deals.length === 0) {
      return <div />;
    }
    return (
      <div className={`${styles.fd} ${styles.web}`}>
        <div className={styles.fdSectionHeader}>
          <span>
            <IconFlash className="icon" />
          </span>
          <span className={styles.title}>{'Flash Sale'}</span>
          <span>
            <IconClock className="icon" />
          </span>
          <span className={styles.countdownTimer}>
            <Countdown endTime={endTime} />
          </span>
          <span className={styles.textViewAll}>
            <Link to={'/flash-sale/' + (isSenMall ? '?mall=1' : '')}>
              {'Xem tất cả'}
            </Link>
            <span className={styles.caretRight}>
              <CaretUp className="icon icon-medium" />
            </span>
          </span>
        </div>
        <div className={styles.fdSectionContent}>
          <Swiper
            setting={this.sliderSetting}
            idSelector={idSelector}
            injectItemClass={'flashdeal'}
          >
            {deals.map((deal, i) => {
              return <FlashSaleItem deal={deal} key={deal.product_id} />;
            })}
          </Swiper>
        </div>
      </div>
    );
  }
}

FlashDeals.propTypes = {
  deals: PropTypes.array.isRequired,
  status: PropTypes.number.isRequired,
  metaData: PropTypes.object.isRequired,
};

export default FlashDeals;
