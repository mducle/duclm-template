import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _get from 'lodash-es/get';

import { formatPrice } from 'sendo-web-sdk/helpers/common';
import { onImgError } from 'sendo-web-sdk/helpers/img';

import defaultImg from 'sendo-buyers-common/images/default.png';

import ProductCardWrap from 'sendo-buyers/components/ProductCardWrap'; //TODO: move this component to common
import Image from 'sendo-buyers/components/Image'; //TODO: move this component to common
import IconDealOrange from 'sendo-buyers-common/components/Icons/IconDealOrange'; //TODO: move this component to common
import IconDealRay from 'sendo-buyers-common/components/Icons/IconDealRay'; //TODO: move this component to common

import discount from './discount.png';
import styles from './flashSaleItem.scss';
import { getPercentDealSold } from './utils';

class FlashSaleItem extends PureComponent {
  static propTypes = {
    deal: PropTypes.object.isRequired,
  };

  render() {
    const deal = this.props.deal;
    const percent = getPercentDealSold(
      deal.sale_stock_number,
      deal.stock_number,
    );
    const inlineWidth = { width: `${percent}%` };
    if (deal.stockStatus !== undefined && deal.stockStatus === 0) {
      return (
        <div className={`${styles.item} ${styles.itemHot} ${styles.web}`}>
          <div className={`${styles.itemImage}`}>
            <div className={styles.imageSquare}>
              <Image
                className="lazyload"
                onError={e => onImgError(e)}
                data-src={deal.img_url_mob}
                alt={deal.name}
                default={defaultImg}
              />
            </div>
            {deal.final_price < deal.price && (
              <div className={`${styles.promotionPercent}`}>
                <div className={`${styles.promotionIcon}`}>
                  <img
                    data-src={discount}
                    className="icon icon-large lazyload"
                    alt=""
                  />
                  <span>{deal.promotion_percent}%</span>
                </div>
              </div>
            )}
          </div>

          <div className={styles.itemInfo}>
            <div className={styles.finalPrice}>
              <strong className={styles.current}>
                {formatPrice(deal.final_price)}
              </strong>
            </div>
            <div className={`${styles.fdProgressBarWrapper}`}>
              <div className={styles.progress}>
                <div className={styles.bar} style={inlineWidth} />
                <div className={`${styles.percent}`}>
                  <span>{deal.stock_description}</span>
                </div>
              </div>
              <span className={styles.iconDeal}>
                <IconDealRay className="icon icon-medium" />
              </span>
            </div>
          </div>
        </div>
      );
    }
    return (
      <ProductCardWrap
        className={`${styles.item} ${styles.web}`}
        product={deal.product_info}
      >
        <div className={`${styles.itemImage}`}>
          <div className={styles.imageSquare}>
            <Image
              className="lazyload"
              onError={e => onImgError(e)}
              data-src={deal.img_url_mob}
              alt={deal.name}
              default={defaultImg}
            />
          </div>
          {deal.final_price < deal.price && (
            <div className={`${styles.promotionPercent}`}>
              <div className={`${styles.promotionIcon}`}>
                <img
                  data-src={discount}
                  className="icon icon-large lazyload"
                  alt=""
                />
                <span>{deal.promotion_percent}%</span>
              </div>
            </div>
          )}
        </div>

        <div className={styles.itemInfo}>
          <div className={styles.finalPrice}>
            <strong className={styles.current}>
              {formatPrice(deal.final_price)}
            </strong>
          </div>
          <div className={`${styles.fdProgressBarWrapper}`}>
            <div className={styles.progress}>
              <div className={styles.bar} style={inlineWidth} />
              <div className={`${styles.percent}`}>
                <span>{deal.stock_description}</span>
              </div>
            </div>
            <span className={styles.iconDeal}>
              <IconDealOrange className="icon icon-medium" />
            </span>
          </div>
        </div>
      </ProductCardWrap>
    );
  }
}

export default FlashSaleItem;
