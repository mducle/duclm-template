import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'sendo-web-sdk/helpers/classnames';

export default class Countdown extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { seconds: 0, minutes: 0, hours: 0 };
  }

  componentDidMount() {
    this.tick();
    this.interval = setInterval(this.tick, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  tick = () => {
    let currentTime = Math.round(new Date().getTime() / 1000.0);
    let endTime = this.props.endTime;
    let remaining = endTime - currentTime;
    if (remaining === 0) {
      window.clearInterval(this.interval);
    }
    let seconds = parseInt(remaining % 60, 10);
    let minutes = parseInt(remaining / 60, 10) % 60;
    let hours = parseInt(remaining / 3600, 10);
    this.setState({
      hours: hours > 0 ? hours : 0,
      minutes: minutes > 0 ? minutes : 0,
      seconds: seconds > 0 ? seconds : 0,
    });
  };

  render() {
    let { hours, minutes, seconds } = this.state;
    seconds = seconds < 10 ? '0' + seconds : seconds;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    hours = hours < 10 ? '0' + hours : hours;
    const countdownClass = cx({
      'sd-countdown': true,
      [this.props.className]: this.props.className ? true : false,
    });
    return (
      <div className={countdownClass}>
        <span className="sd-hour">{hours}</span>
        <span className="sd-colon">:</span>
        <span className="sd-minute">{minutes}</span>
        <span className="sd-colon">:</span>
        <span className="sd-second">{seconds}</span>
      </div>
    );
  }
}

Countdown.propTypes = {
  endTime: PropTypes.number.isRequired, // endTime in seconds
  className: PropTypes.string,
};

Countdown.defaultProps = {
  endTime: null,
};
