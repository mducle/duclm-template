import _get from 'lodash-es/get';

import apiStatus from 'sendo-web-sdk/apiStatus';

import createDataType from 'sendo-data-provider/createDataType';
import createDataConnecter from 'sendo-data-provider/createDataConnecter';
import createDataFetcher from 'sendo-data-provider/createDataFetcher';

import { HOME_ROUTE_NAME } from 'sendo-buyers/routes/homeRoute';
import { MALL_HOME_ROUTE_NAME } from 'sendo-buyers/routes/mallHomeRoute';

export const FLASH_SALE_INFO = createDataType({
  name: 'flashSaleInfo',
});

export const connecter = createDataConnecter({
  type: FLASH_SALE_INFO,
});

export const fetcher = createDataFetcher({
  name: 'FETCH_FLASH_SALE_INFO',
  url: '/m/wap_v2/home/flash-deal',
  id: (props, location) => {
    if (location.routeName === HOME_ROUTE_NAME) {
      return 'flashsale-home';
    }
    if (location.routeName === MALL_HOME_ROUTE_NAME) {
      return 'flashsale-mall';
    }
    return false;
  },
  query: props =>
    props.isSenMall
      ? {
          s: 10,
          shop_type: 2,
        }
      : { s: 10 },
  onPending: async props => {
    return {
      [FLASH_SALE_INFO]: FLASH_SALE_INFO.create([
        {
          status: apiStatus.PENDING,
        },
      ]),
    };
  },
  onResponse: async body => {
    const info = _get(body, ['data', 'data', 'list'], []);
    const endTime = _get(body, ['data', 'data', 'end_time'], 0);
    return {
      [FLASH_SALE_INFO]: FLASH_SALE_INFO.create([
        {
          status: apiStatus.SUCCESS,
          data: info,
          metaData: {
            endTime,
          },
        },
      ]),
    };
  },
  onError: async error => {
    return {
      [FLASH_SALE_INFO]: FLASH_SALE_INFO.create([
        {
          status: apiStatus.ERROR,
          data: null,
        },
      ]),
    };
  },
  cacheTime: 300000,
});
