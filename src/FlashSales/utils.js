export const getPercentDealSold = (saleStockNumber, stockNumber) => {
  return Math.abs(stockNumber - saleStockNumber) / saleStockNumber * 100;
};
