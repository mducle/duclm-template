/*
 * File: library.js
 * File Created: Sunday, 26th August 2018 5:47:33 pm
 * Last Modified: Sunday, 26th August 2018 6:02:37 pm
 * Modified By: Le Minh Duc<duclm@sendo.vn>
 */

import { compose } from 'recompose';
import React, { PureComponent } from 'react';

import _get from 'lodash-es/get';

import apiStatus from 'sendo-web-sdk/apiStatus';

import withDataProvider from 'sendo-data-provider/withDataProvider';
import withDataFetcher from 'sendo-data-provider/withDataFetcher';

import { connecter, fetcher } from './FlashSales/dataConfig';
import FlashSales from './FlashSales';

class SendoFlashSale extends PureComponent {
  render() {
    const {
      [connecter.data]: data,
      [connecter.status]: status,
      [connecter.metaData]: metaData,
      isSenMall,
    } = this.props;
    if (status === apiStatus.PENDING) {
      //TODO: render placeholder
      return null;
    }
    if (status === apiStatus.SUCCESS) {
      return (
        <FlashSales deals={data} metaData={metaData} isSenMall={isSenMall} />
      );
    }
    return null;
  }
}

const withFlashSaleDataProvider = withDataProvider(connecter);
const withFlashSaleDataFetcher = withDataFetcher(fetcher);

export default compose(
  withFlashSaleDataProvider,
  withFlashSaleDataFetcher,
)(SendoFlashSale);
