// /*
//  * File: client.js
//  * File Created: Sunday, 26th August 2018 5:47:18 pm
//  * Last Modified: Sunday, 26th August 2018 5:47:53 pm
//  * Modified By: Le Minh Duc<duclm@sendo.vn>
//  */

import React from 'react';
import Sendo from 'sendo-buyers/library';
import SendoFlashSale from './library';

const sendo = new Sendo();

sendo.registerComponent(() => {
  return (
    <div>
      <SendoFlashSale />
    </div>
  );
});

sendo.render();
