# Sendo React JS Boilerplate

## Getting started

```bash
mkdir new-project
cd new-project
git clone https://gitlab.sendo.vn/sendo-workspace/sendo-buyers-component-template.git .
rm -rf .git
git init
git add .
git commit -m "init project"
yarn install
cp .env.example .env
yarn build:web
yarn start
```
